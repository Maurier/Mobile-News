var Mioga = Backbone.Model.extend ({
	protocol: null,
	host:     null,
	base:     null,
	instance: null,
	group:    null,
	logged:   null,
	router:   null,
	defaults: {
		protocol: 'https'
	},

	initialize: function (args) {
		var that = this;

		// Create and start router
		var Router  = Backbone.Router.extend ({});
		this.router = new Router ();
		Backbone.history.start ();

		// Check connection status
		// Query Magellan GetNodes as login system currently can not speak JSON
		$.ajax ({
			type: 'POST',
			url: this.get ('private_bin_uri') + '/Magellan/GetNodes.json',
			crossDomain: true,
			xhrFields: {
				withCredentials: true
			},
			data: {
				node: this.get ('home_uri')
			},
			success: function (data) {
				if (typeof (data) === 'object') {
					// Server returned JSON, user is currently logged-in
					that.set ('logged', true);
				}
				else {
					// Server returned HTML (login form), user is not currently logged-in
					that.set ('logged', false);
				}
			},
			error: function (data) {
				// Server returned an error, user is not currently logged-in
				that.set ('logged', false);
			}
		});
	},
	get: function (attr) {
		switch (attr) {
			case 'private_bin_uri':
				return ((this.get ('protocol') + '://' + this.get ('host') + '/' + this.get ('base') + '/' + this.get ('instance') + '/bin/' + this.get ('group')).replace (/\/\//g, '/'));
				break;
			case 'base_uri':
				return (('/' + this.get ('base') + '/' + this.get ('instance')).replace (/\/\//g, '/'));
				break;
			case 'home_uri':
				return (('/' + this.get ('base') + '/' + this.get ('instance') + '/home/' + this.get ('group')).replace (/\/\//g, '/'));
				break;
			case 'login_uri':
				return ((this.get ('protocol') + '://' + this.get ('host') + '/' + this.get ('base') + '/login/DisplayMain?target=' + this.get ('base_uri')).replace (/\/\//g, '/'));
				break;
			default:
				return (Backbone.Model.prototype.get.call (this, attr));
		}
	}
});
